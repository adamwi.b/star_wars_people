part of 'people_bloc.dart';

abstract class PeopleEvent extends Equatable {
  const PeopleEvent();

  @override
  List<Object> get props => [];
}

class GetPeoples extends PeopleEvent {
  final String query;

  const GetPeoples({this.query});
}
