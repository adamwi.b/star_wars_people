part of 'auth_bloc.dart';

@immutable
abstract class AuthEvent extends Equatable{}

class AppLogoutRequested extends AuthEvent {
  @override
  
  List<Object> get props => [];
}

class AppUserChanged extends AuthEvent {
  @visibleForTesting
  AppUserChanged(this.user);

  final User user;

  @override
  List<Object> get props => [user];
}