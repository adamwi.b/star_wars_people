import 'package:firebase_core/firebase_core.dart';
import 'package:flow_builder/flow_builder.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:try_star_wars/modules/auth/pages/login_page.dart';
import 'package:try_star_wars/modules/home/blocs/bloc/people_bloc.dart';
import 'package:try_star_wars/modules/home/data/repositories/people_repositories.dart';
import 'package:try_star_wars/modules/menu/menu.dart';
import 'package:try_star_wars/modules/splash/splash_screen.dart';

import 'modules/auth/blocs/auth/auth_bloc.dart';
import 'modules/auth/data/repositories/authentication_repository.dart';

Future<void> main() async {
  Bloc.observer = AppBlocObserver();
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  final authenticationRepository = AuthenticationRepository();
  final peopleRepository = PeopleRepository();
  await authenticationRepository.user.first;
  runApp(MultiRepositoryProvider(
    providers: [
      RepositoryProvider.value(value: authenticationRepository),
      RepositoryProvider.value(value: peopleRepository)
    ],
    child: MultiBlocProvider(
      providers: [
        BlocProvider(
          create: (context) =>
              AuthBloc(authenticationRepository: authenticationRepository),
        ),
        BlocProvider(
          create: (context) =>
              PeopleBloc(repository: peopleRepository),
        ),
      ],
      child: const MyApp(),
    ),
  ));
}

class MyApp extends StatelessWidget {
  const MyApp({
    Key key,
  }) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Star Wars',
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        home: const SplashScreen());
  }
}

class App extends StatelessWidget {
  const App({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FlowBuilder<AppStatus>(
      state: context.select((AuthBloc bloc) => bloc.state.status),
      onGeneratePages: onGenerateAppViewPages,
    );
  }
}

List<Page> onGenerateAppViewPages(AppStatus state, List<Page<dynamic>> pages) {
  switch (state) {
    case AppStatus.authenticated:
      return [MenuPage.page()];
    case AppStatus.unauthenticated:
    default:
      return [LoginPage.page()];
  }
}

class AppBlocObserver extends BlocObserver {
  @override
  void onEvent(Bloc bloc, Object event) {
    super.onEvent(bloc, event);
    print(event);
  }

  @override
  void onError(BlocBase bloc, Object error, StackTrace stackTrace) {
    print(error);
    super.onError(bloc, error, stackTrace);
  }

  @override
  void onChange(BlocBase bloc, Change change) {
    super.onChange(bloc, change);
    print(change);
  }

  @override
  void onTransition(Bloc bloc, Transition transition) {
    super.onTransition(bloc, transition);
    print(transition);
  }
}
