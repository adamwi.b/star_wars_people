import 'dart:async';

import 'package:flutter/material.dart';
import 'package:try_star_wars/main.dart';
import 'package:try_star_wars/modules/auth/pages/login_page.dart';
import 'package:try_star_wars/utilities/colors.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key key}) : super(key: key);

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    super.initState();
    startSplashScreen();
  }

  startSplashScreen() async {
    var duration = const Duration(seconds: 6);
    return Timer(duration, () {
      Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (_) {
        return const App();
      }));
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: colorPrimary,
      body: Center(
          child: Image.network(
        "https://upload.wikimedia.org/wikipedia/commons/5/5a/Star_Wars_Logo..png",
        height: 200,
      )),
    );
  }
}
