import 'package:flutter/material.dart';
import 'package:try_star_wars/utilities/colors.dart';

class FavoritePage extends StatelessWidget {
  const FavoritePage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: colorBg,
      body: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
        const SizedBox(
          height: 40,
        ),
        const Padding(
          padding: EdgeInsets.symmetric(horizontal: 20),
          child: Text("Your Favorite",
              style: TextStyle(fontSize: 20, color: colorText)),
        ),
        Expanded(
          child: ListView(
            children: [
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 20),
                child: Row(
                  children: [
                    Expanded(
                      child: Container(
                          height: 90,
                          decoration: BoxDecoration(
                              color: colorBgLight,
                              boxShadow: [
                                BoxShadow(
                                    blurRadius: 2,
                                    spreadRadius: 1,
                                    color: Colors.grey[200])
                              ],
                              borderRadius: const BorderRadius.only(
                                  topLeft: Radius.circular(12),
                                  bottomRight: Radius.circular(12))),
                          child: Padding(
                            padding: const EdgeInsets.all(12),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: const [
                                    Text("Luke Skywalker",
                                        style: TextStyle(color: colorText)),
                                    Text("height: 172",
                                        style: TextStyle(color: colorText)),
                                    Text("mass: 77",
                                        style: TextStyle(color: colorText)),
                                    Text("male",
                                        style: TextStyle(color: colorText)),
                                  ],
                                ),
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: const [
                                    Text("hair color: blond",
                                        style: TextStyle(color: colorText)),
                                    Text("skin color: fair",
                                        style: TextStyle(color: colorText)),
                                    Text("eye color: blue",
                                        style: TextStyle(color: colorText)),
                                    Text("birth year : 19BBY",
                                        style: TextStyle(color: colorText)),
                                  ],
                                )
                              ],
                            ),
                          )),
                    ),
                    const SizedBox(
                      width: 8,
                    ),
                    const Icon(Icons.delete, color: Colors.red)
                  ],
                ),
              )
            ],
          ),
        )
      ]),
    );
  }
}
