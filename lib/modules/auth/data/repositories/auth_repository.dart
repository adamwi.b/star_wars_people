// import 'dart:async';
// import 'dart:convert';
// import 'package:firebase_auth/firebase_auth.dart';
// import 'package:firebase_core/firebase_core.dart';
// import 'package:google_sign_in/google_sign_in.dart';
// import '../models/user.dart' as user;

// class UserRepository {
//   final FirebaseAuth _firebaseAuth;
//   final GoogleSignIn _googleSignIn;

//   final AuthApiProvider apiProvider;

//   String smsVerificationCode = "";

//   User user;

//   bool isUserSigned;

//   UserRepository(
//       {FirebaseAuth firebaseAuth,
//       AuthApiProvider apiProvider,
//       GoogleSignIn googleSignin,
//       User user})
//       : _firebaseAuth = firebaseAuth ?? FirebaseAuth.instance,
//         _googleSignIn = googleSignin ?? GoogleSignIn(),
//         user = user ?? User(),
//         apiProvider = apiProvider ?? AuthApiProvider();

//   Future<User> signInWithGoogle() async {
//     final GoogleSignInAccount googleUser = await _googleSignIn.signIn();
//     final GoogleSignInAuthentication googleAuth =
//         await googleUser.authentication;
//     final AuthCredential credential = GoogleAuthProvider.getCredential(
//       accessToken: googleAuth.accessToken,
//       idToken: googleAuth.idToken,
//     );
//     await _firebaseAuth.signInWithCredential(credential);
//     return _firebaseAuth.currentUser();
//   }

//   Future<void> signInWithCredentials(String email, String password) {
//     return _firebaseAuth.signInWithEmailAndPassword(
//       email: email,
//       password: password,
//     );
//   }

//   Future<AuthResult> registerWithEmailAndPassword(
//       {String email, String password}) async {
//     FirebaseApp app = await FirebaseApp.configure(
//         name: 'Secondary', options: await FirebaseApp.instance.options);
//     var result = await FirebaseAuth.fromApp(app)
//         .createUserWithEmailAndPassword(email: email, password: password);
//     await result.user.sendEmailVerification();
//     return result;
//   }

//   Future<void> sendResetPasswordLink({String email}) async {
//     return await _firebaseAuth.sendPasswordResetEmail(email: email);
//   }

//   Future<FirebasePhoneResponse> signInWithPhoneCredential(
//       AuthCredential credential) async {
//     FirebasePhoneResponse response = new FirebasePhoneResponse();

//     var authResult = await _firebaseAuth.signInWithCredential(credential);
//     if (authResult != null) {
//       IdTokenResult token = await authResult.user.getIdToken();
// //      response = await apiProvider.loginWithPhone(token.token);
//     }

//     ///Test Mode
//     response.status = "success";

//     return response;
//   }

//   Future<FirebasePhoneResponse> registerWithPhoneCredential(
//       AuthCredential credential) async {
//     FirebasePhoneResponse response = new FirebasePhoneResponse();

//     var authResult = await _firebaseAuth.signInWithCredential(credential);
//     if (authResult != null) {
//       IdTokenResult token = await authResult.user.getIdToken();
// //      response = await apiProvider.registerWithPhone(token.token);
//     }

//     ///Test Mode
//     response.status = "success";

//     return response;
//   }

//   Future<void> signUp({String email, String password}) async {
//     return await _firebaseAuth.createUserWithEmailAndPassword(
//       email: email,
//       password: password,
//     );
//   }

//   Future<void> signOut() async {
//     return Future.wait([
//       _firebaseAuth.signOut(),
//       _googleSignIn.signOut(),
//     ]);
//   }

//   Future<bool> isSignedIn() async {
//     final currentUser = await _firebaseAuth.currentUser();
//     return currentUser != null;
//   }

//   Future<String> getUser() async {
//     return (await _firebaseAuth.currentUser()).email;
//   }

//   Future<VerifyPhoneFirebase> verifyPhoneNumber(String phoneNumber,
//       {String nationality = PrefixPhoneNum.id}) async {
//     String completePhoneNum = nationality + phoneNumber;
//     VerifyPhoneFirebase result = VerifyPhoneFirebase();
//     try {
//       await _firebaseAuth.verifyPhoneNumber(
//           phoneNumber: completePhoneNum,
//           timeout: Duration(milliseconds: 60),
//           verificationCompleted: null,
//           verificationFailed: (authException) {
//             result.exception = authException.code;
//             print(authException.message);
//           },
//           codeAutoRetrievalTimeout: (verificationId) {
//             print("VERIF id " + verificationId);
//             result.verificationId = verificationId;
//           },
//           codeSent: (verificationId, [code]) {
//             print("VERIF id " + verificationId);
//             result.verificationId = verificationId;
//           });
//     } catch (e) {
//       print(e.toString());
//     }
//     return result;
//   }
// }

// class AuthApiProvider {
//   Network network = Network();

//   Future<FirebasePhoneResponse> registerWithPhone(String token) async {
//     final String url =
//         "$BASE_URL_API/$API_VERSION/$AUTH_MODULES/$FIREBASE_METHOD/$PHONE_REGISTER";
//     print(token);
//     final response = await network.postRequestWithoutToken(
//         url, jsonEncode({"token": token, "register_as": "customer"}));
//     print(response.body);
//     if (response.statusCode == 200) {
//       return FirebasePhoneResponse.fromRawJson(response.body);
//     } else {
//       return null;
//     }
//   }

//   Future<FirebasePhoneResponse> loginWithPhone(String token) async {
//     final String url =
//         "$BASE_URL_API/$API_VERSION/$AUTH_MODULES/$FIREBASE_METHOD/$PHONE_LOGIN";
//     print(token);
//     final response = await network.postRequestWithoutToken(
//         url, jsonEncode({"token": token}));
//     print(response.body);
//     if (response.statusCode == 200) {
//       return FirebasePhoneResponse.fromRawJson(response.body);
//     } else {
//       return null;
//     }
//   }
// }
