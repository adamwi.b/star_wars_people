import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter/material.dart';
import 'package:try_star_wars/modules/home/data/models/people_response.dart';
import 'package:try_star_wars/modules/home/detail_page.dart';

import 'blocs/bloc/people_bloc.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  void initState() {
    super.initState();
    context.read<PeopleBloc>().add(const GetPeoples());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[850],
      body: SingleChildScrollView(
        child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
          const SizedBox(
            height: 40,
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20),
            child: Row(
              children: [
                Expanded(
                  child: TextFormField(
                    style: const TextStyle(color: Colors.white),
                    onChanged: (value) {
                      context.read<PeopleBloc>().add(GetPeoples(query: value));
                    },
                    cursorColor: Colors.white,
                    decoration: InputDecoration(
                        focusedBorder: OutlineInputBorder(
                            borderSide:
                                BorderSide(color: Colors.grey[50], width: 2),
                            borderRadius: BorderRadius.circular(20)),
                        enabledBorder: OutlineInputBorder(
                            borderSide:
                                BorderSide(color: Colors.grey[50], width: 2),
                            borderRadius: BorderRadius.circular(20)),
                        hintText: "Search people ...",
                        hintStyle: const TextStyle(color: Colors.white),
                        labelText: "Search people. ",
                        labelStyle: const TextStyle(color: Colors.white),
                        border: OutlineInputBorder(
                            borderSide:
                                BorderSide(color: Colors.grey[50], width: 2),
                            borderRadius: BorderRadius.circular(20))),
                  ),
                ),
                const SizedBox(
                  width: 12,
                ),
                Container(
                  height: 40,
                  width: 40,
                  decoration: const BoxDecoration(
                      shape: BoxShape.circle, color: Colors.grey),
                  child: const Icon(
                    Icons.filter_alt_rounded,
                    size: 28,
                    color: Colors.yellow,
                  ),
                )
              ],
            ),
          ),
          const SizedBox(
            height: 20,
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20),
            child: Row(
              children: [
                Container(
                  height: 20,
                  width: 8,
                  decoration: const BoxDecoration(
                      color: Colors.yellow,
                      borderRadius: BorderRadius.only(
                          topRight: Radius.circular(4),
                          bottomRight: Radius.circular(4))),
                ),
                const SizedBox(
                  width: 12,
                ),
                const Text("List View",
                    style: TextStyle(fontSize: 20, color: Colors.white)),
              ],
            ),
          ),
          const SizedBox(
            height: 20,
          ),
          BlocBuilder<PeopleBloc, PeopleState>(
            builder: (context, state) {
              if (state is PeopleSuccess) {
                return Column(
                  children: [
                    PeopleListView(
                      response: state.result,
                    ),
                    PeopleGridView(
                      response: state.result,
                    ),
                  ],
                );
              }
              return const Center(
                child: SizedBox(
                  height: 40,
                  width: 40,
                  child: CircularProgressIndicator(),
                ),
              );
            },
          ),
          const SizedBox(
            height: 20,
          ),
        ]),
      ),
    );
  }
}

class PeopleGridView extends StatefulWidget {
  const PeopleGridView({
    Key key,
    this.response,
  }) : super(key: key);

  final PeopleResponse response;

  @override
  State<PeopleGridView> createState() => _PeopleGridViewState();
}

class _PeopleGridViewState extends State<PeopleGridView> {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const SizedBox(
          height: 20,
        ),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20),
          child: Row(
            children: [
              Container(
                height: 20,
                width: 8,
                decoration: const BoxDecoration(
                    color: Colors.yellow,
                    borderRadius: BorderRadius.only(
                        topRight: Radius.circular(4),
                        bottomRight: Radius.circular(4))),
              ),
              const SizedBox(
                width: 12,
              ),
              const Text("Grid View",
                  style: TextStyle(fontSize: 20, color: Colors.white)),
            ],
          ),
        ),
        SizedBox(
          height: 500,
          child: GridView.builder(
            shrinkWrap: true,
            primary: false,
            padding: const EdgeInsets.all(20),
            gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 2, crossAxisSpacing: 10, mainAxisSpacing: 10),
            itemCount: widget.response.results.length,
            itemBuilder: (context, i) =>
                buildPeopleGridView(widget.response.results[i]),
          ),
        ),
      ],
    );
  }

  Widget buildPeopleGridView(Result result) {
    return InkWell(
      onTap: () {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => DetailPeople(
                      item: result,
                    )));
      },
      child: Container(
          height: 60,
          decoration: BoxDecoration(
              color: Colors.grey[700],
              boxShadow: const [
                BoxShadow(blurRadius: 2, spreadRadius: 1, color: Colors.white)
              ],
              borderRadius: const BorderRadius.only(
                  topLeft: Radius.circular(12),
                  bottomRight: Radius.circular(12))),
          child: Padding(
            padding: const EdgeInsets.all(12),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  result.name,
                  style: const TextStyle(
                      fontSize: 18,
                      color: Colors.white,
                      fontWeight: FontWeight.w600),
                ),
                Text(result.gender.toString().split(".").last,
                    style: const TextStyle(
                        fontSize: 15,
                        color: Colors.white,
                        fontWeight: FontWeight.w600)),
                Text(result.birthYear,
                    style: const TextStyle(
                        fontSize: 15,
                        color: Colors.white,
                        fontWeight: FontWeight.w600)),
              ],
            ),
          )),
    );
  }
}

class PeopleListView extends StatefulWidget {
  const PeopleListView({
    Key key,
    @required this.response,
  }) : super(key: key);

  final PeopleResponse response;

  @override
  State<PeopleListView> createState() => _PeopleListViewState();
}

class _PeopleListViewState extends State<PeopleListView> {
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 100,
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 4),
        child: ListView.separated(
          physics: const BouncingScrollPhysics(),
          shrinkWrap: true,
          scrollDirection: Axis.horizontal,
          itemBuilder: (context, index) =>
              _buildPeopleItem(widget.response.results[index]),
          separatorBuilder: (context, index) => const SizedBox(
            width: 10,
          ),
          itemCount: widget.response.results.length,
        ),
      ),
    );
  }

  Widget _buildPeopleItem(Result item) {
    return InkWell(
      onTap: () => Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => DetailPeople(
                    item: item,
                  ))),
      child: Container(
          height: 90,
          decoration: BoxDecoration(
              color: Colors.grey[700],
              boxShadow: [
                BoxShadow(
                    blurRadius: 2, spreadRadius: 1, color: Colors.grey[400])
              ],
              borderRadius: const BorderRadius.only(
                  topLeft: Radius.circular(12),
                  bottomRight: Radius.circular(12))),
          child: Padding(
            padding: const EdgeInsets.all(12),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  item.name,
                  style: const TextStyle(
                      fontSize: 18,
                      color: Colors.white,
                      fontWeight: FontWeight.w600),
                ),
                Text(item.gender.toString().split(".").last,
                    style: const TextStyle(
                        fontSize: 15,
                        color: Colors.white,
                        fontWeight: FontWeight.w600)),
                Text(item.birthYear,
                    style: const TextStyle(
                        fontSize: 15,
                        color: Colors.white,
                        fontWeight: FontWeight.w600)),
              ],
            ),
          )),
    );
  }
}
