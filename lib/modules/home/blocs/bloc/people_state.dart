part of 'people_bloc.dart';

abstract class PeopleState extends Equatable {
  const PeopleState();

  @override
  List<Object> get props => [];
}

class PeopleInitial extends PeopleState {}

class PeopleLoading extends PeopleState {}

class PeopleSuccess extends PeopleState {
  final PeopleResponse result;

  const PeopleSuccess(this.result);
  @override
  List<Object> get props => [result];
}

class PeopleError extends PeopleState {}
