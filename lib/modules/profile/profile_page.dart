import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:try_star_wars/modules/auth/data/repositories/authentication_repository.dart';
import 'package:try_star_wars/utilities/colors.dart';

class ProfilePage extends StatelessWidget {
  const ProfilePage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: colorBg,
      body: ListView(
        children: [
          const SizedBox(
            height: 20,
          ),
          // Padding(
          //   padding: const EdgeInsets.symmetric(horizontal: 30),
          //   child: Row(
          //     mainAxisAlignment: MainAxisAlignment.spaceBetween,
          //     children: const [Icon(Icons.arrow_back), Icon(Icons.favorite)],
          //   ),
          // ),
          const SizedBox(
            height: 20,
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 30),
            child: Container(
              height: 200,
              decoration: BoxDecoration(
                boxShadow: [
                  BoxShadow(
                      blurRadius: 3, spreadRadius: 1, color: Colors.grey[50])
                ],
                borderRadius: BorderRadius.circular(12),
                color: colorBgLight,
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const SizedBox(
                    height: 12,
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 20),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          height: 80,
                          width: 80,
                          decoration: const BoxDecoration(
                              color: Colors.grey, shape: BoxShape.circle),
                          child: ClipRRect(
                            borderRadius: BorderRadius.circular(80),
                            child: Image.network(
                              context
                                  .read<AuthenticationRepository>()
                                  .currentUser
                                  .photo,
                            ),
                          ),
                        ),
                        const Icon(Icons.edit, size: 30, color: colorText)
                      ],
                    ),
                  ),
                  const SizedBox(
                    height: 12,
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 20),
                    child: Text(
                      context.read<AuthenticationRepository>().currentUser.name,
                      style: const TextStyle(fontSize: 18, color: colorText),
                    ),
                  ),
                  const SizedBox(
                    height: 4,
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 20),
                    child: Text(
                        context
                            .read<AuthenticationRepository>()
                            .currentUser
                            .email,
                        style: const TextStyle(fontSize: 18, color: colorText)),
                  )
                ],
              ),
            ),
          ),
          const SizedBox(
            height: 180,
          ),
          InkWell(
            onTap: () {
              context.read<AuthenticationRepository>().logOut();
            },
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 30),
              child: Container(
                height: 46,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(30),
                    color: Colors.white,
                    border: Border.all(color: Colors.red, width: 1.2)),
                child: const Center(
                    child: Text(
                  "Sign Out",
                  style: TextStyle(
                      color: Colors.red,
                      fontSize: 16,
                      fontWeight: FontWeight.w600),
                )),
              ),
            ),
          )
        ],
      ),
    );
  }
}
