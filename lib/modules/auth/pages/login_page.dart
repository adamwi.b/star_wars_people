import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:try_star_wars/modules/auth/blocs/login/login_cubit.dart';
import 'package:try_star_wars/modules/auth/data/repositories/authentication_repository.dart';
import 'package:try_star_wars/utilities/colors.dart';
import 'package:formz/formz.dart';

class LoginPage extends StatelessWidget {
  const LoginPage({Key key}) : super(key: key);
static Page page() => const MaterialPage<void>(child: LoginPage());
  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => LoginCubit(context.read<AuthenticationRepository>()),
      child: const LoginScreen(),
    );
  }
}

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key key}) : super(key: key);

  

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: colorSecondary,
      body: BlocListener<LoginCubit, LoginState>(
        listener: (context, state) {
          if (state.status.isSubmissionFailure) {
            ScaffoldMessenger.of(context)
              ..hideCurrentSnackBar()
              ..showSnackBar(
                SnackBar(
                  content:
                      Text(state.errorMessage ?? 'Authentication Failure'),
                ),
              );
          }
        },
        child: Column(
          children: [
            const SizedBox(
              height: 120,
            ),
            Image.network(
              "https://pngimg.com/uploads/star_wars_logo/star_wars_logo_PNG34.png",
              height: 200,
            ),
            const Spacer(),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 70),
              child: InkWell(
                onTap: () {
                  context.read<LoginCubit>().logInWithGoogle();
                },
                child: Container(
                  height: 46,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(20),
                  ),
                  child: Center(
                    child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 20),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Image.network(
                            "https://upload.wikimedia.org/wikipedia/commons/thumb/5/53/Google_%22G%22_Logo.svg/2048px-Google_%22G%22_Logo.svg.png",
                            height: 28,
                          ),
                          const Text(
                            "Sign in With Google",
                            style: TextStyle(fontSize: 19),
                          )
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            ),
            const SizedBox(height: 80)
          ],
        ),
      ),
    );
  }
}
