import 'dart:async';

import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';
import 'dart:io' as io;

import 'package:try_star_wars/modules/home/data/models/people_response.dart';

class Offline {
  static Database _db;
  static const String DB_NAME = 'star.db';
  static const String TABLE_NAME = "fav";
  static const String ID = "id";
  static const String NAME = "name";
  static const String GENDER = "gender";
  static const String BIRTHDAY = "birthday";

  Future<Database> get db async {
    if (null != _db) {
      return _db;
    }
    _db = await initDb();
    return _db;
  }

  initDb() async {
    io.Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, DB_NAME);
    var db = await openDatabase(path, version: 1, onCreate: _onCreate);
    return db;
  }

  FutureOr<void> _onCreate(Database db, int version) async {
    await db.execute(""
        "CREATE TABLE IF NOT EXISTS $TABLE_NAME("
        "$NAME TEXT, "
        "$GENDER TEXT, "
        "$BIRTHDAY TEXT "
        ")");
  }

  Future<void> insertFav(Result item) async {
    await (await db).rawInsert(
        "INSERT INTO $TABLE_NAME("
        "$NAME, "
        "$GENDER, "
        "$BIRTHDAY "
        ") VALUES(?,?,?)",
        [item.name, item.gender, item.birthYear]);
  }
}
