import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:try_star_wars/modules/home/data/models/people_response.dart';
import 'package:try_star_wars/modules/home/data/repositories/people_repositories.dart';

part 'people_event.dart';
part 'people_state.dart';

class PeopleBloc extends Bloc<PeopleEvent, PeopleState> {
  PeopleBloc({@required this.repository}) : super(PeopleInitial()) {
    on<GetPeoples>(_getPeople);
  }

  final PeopleRepository repository;

  void _getPeople(GetPeoples event, Emitter<PeopleState> emit) async {
    emit(PeopleLoading());
    var result = await repository.getPeople(event.query ?? "");
    emit(PeopleSuccess(result));
  }
}
