import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

const Color colorPrimary = Color.fromRGBO(19, 12, 14, 255);

const Color colorSecondary = Color.fromRGBO(211, 211, 211, 0.25);

const Color colorLight = Colors.yellow;
const Color colorText = Colors.white;

Color colorBg = Colors.grey[850];
Color colorBgLight = Colors.grey[700];
