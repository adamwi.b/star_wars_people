import 'package:flutter/material.dart';
import 'package:try_star_wars/utilities/colors.dart';

import 'data/models/people_response.dart';

class DetailPeople extends StatelessWidget {
  const DetailPeople({Key key, this.item}) : super(key: key);
  final Result item;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: colorBg,
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const SizedBox(
            height: 40,
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 30),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                InkWell(
                    onTap: () => Navigator.pop(context),
                    child: const Icon(Icons.arrow_back, color: colorText)),
                const Icon(
                  Icons.favorite,
                  color: Colors.red,
                )
              ],
            ),
          ),
          const SizedBox(
            height: 20,
          ),
          const Padding(
            padding: EdgeInsets.symmetric(horizontal: 30),
            child: Text(
              "Detail People",
              style: TextStyle(color: colorText, fontSize: 18),
            ),
          ),
          const SizedBox(
            height: 20,
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 30),
            child: Container(
              height: 180,
              decoration: BoxDecoration(
                boxShadow: [
                  BoxShadow(
                      blurRadius: 3, spreadRadius: 1, color: Colors.grey[50])
                ],
                borderRadius: BorderRadius.circular(12),
                color: colorBgLight,
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const SizedBox(
                        height: 12,
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 20),
                        child: Text(
                          item.name,
                          style:
                              const TextStyle(fontSize: 18, color: colorText),
                        ),
                      ),
                      const SizedBox(
                        height: 4,
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 20),
                        child: Text(
                            "Gender: " + item.gender.toString().split(".").last,
                            style: const TextStyle(
                                fontSize: 18, color: colorText)),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 20),
                        child: Text("Height: " + item.height,
                            style: const TextStyle(
                                fontSize: 18, color: colorText)),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 20),
                        child: Text("Mass: " + item.mass,
                            style: const TextStyle(
                                fontSize: 18, color: colorText)),
                      ),
                    ],
                  ),
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const SizedBox(
                          height: 12,
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 20),
                          child: Text(
                            "Hair Color: " + item.hairColor,
                            style:
                                const TextStyle(fontSize: 18, color: colorText),
                          ),
                        ),
                        const SizedBox(
                          height: 4,
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 20),
                          child: Text("Skin Color: " + item.skinColor,
                              style: const TextStyle(
                                  fontSize: 18, color: colorText)),
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 20),
                          child: Text("Eye Color: " + item.eyeColor,
                              style: const TextStyle(
                                  fontSize: 18, color: colorText)),
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 20),
                          child: Text("Birth Year: " + item.birthYear,
                              style: const TextStyle(
                                  fontSize: 18, color: colorText)),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
