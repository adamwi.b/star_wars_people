import 'package:flutter/material.dart';
import 'package:try_star_wars/modules/favorite/favorite_page.dart';
import 'package:try_star_wars/modules/home/home_page.dart';
import 'package:try_star_wars/modules/profile/profile_page.dart';

class MenuPage extends StatefulWidget {
  const MenuPage({Key key}) : super(key: key);
  static Page page() => const MaterialPage<void>(child: MenuPage());
  @override
  _MenuPageState createState() => _MenuPageState();
}

class _MenuPageState extends State<MenuPage> {
  int _selectedIndex = 0;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: BottomAppBar(
        child: Row(
          children: [
            buildItemAppbar(Icons.home, 0),
            buildItemAppbar(Icons.favorite, 1),
            buildItemAppbar(Icons.person, 2),
          ],
        ),
      ),
      body: Stack(
        children: [
          Offstage(
            offstage: _selectedIndex != 0,
            child: TickerMode(
              enabled: _selectedIndex == 0,
              child: const HomePage(),
            ),
          ),
          Offstage(
            offstage: _selectedIndex != 1,
            child: TickerMode(
              enabled: _selectedIndex == 1,
              child: const FavoritePage(),
            ),
          ),
          Offstage(
            offstage: _selectedIndex != 2,
            child: TickerMode(
              enabled: _selectedIndex == 2,
              child: const ProfilePage(),
            ),
          )
        ],
      ),
    );
  }

  Widget buildItemAppbar(IconData icon, int index) {
    return GestureDetector(
      onTap: () {
        setState(() {
          _selectedIndex = index;
        });
      },
      child: Container(
        color: Colors.grey[900],
        height: 50,
        width: MediaQuery.of(context).size.width / 3,

        // color: index == _selectedItemIndex ? Colors.green : Colors.white),
        child: Icon(icon,
            size: index == _selectedIndex ? 28 : 24,
            color: index == _selectedIndex ? Colors.yellow : Colors.grey),
      ),
    );
  }
}
