import 'dart:convert';
import 'dart:io';

import 'package:http/http.dart';
import 'package:try_star_wars/modules/home/data/models/people_response.dart';

class PeopleRepository {
  Future<PeopleResponse> getPeople(String search) async {
    // print("ENDPOINT URL : ${NetworkUrl.cekKategori()}");
    try {
      final response = await Client()
          .get(Uri.parse("https://swapi.dev/api/people/?search=$search"));
      // print("RESPONSE STATUS CODE : Article ${response.statusCode}");
      if (response.statusCode == 200) {
        return peopleResponseFromJson(response.body);
      } else {
        return PeopleResponseFailure("An unkown error");
      }
    } catch (e) {
      return PeopleResponseFailure("An unkown error");
    }
  }
}

class PeopleResponseFailure extends PeopleResponse {
  final String error;

  PeopleResponseFailure(this.error);
}
